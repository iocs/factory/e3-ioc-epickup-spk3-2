require essioc
require xtpico
require iocmetadata

iocshLoad("$(essioc_DIR)/common_config.iocsh")

epicsEnvSet("R_TMP100",             ":Temp")
epicsEnvSet("R_M24M02",             ":Eeprom")
epicsEnvSet("R_TCA9555",            ":IOExp")
epicsEnvSet("R_LTC2991",            ":VMon")
epicsEnvSet("R_AD527X",             ":Res")

epicsEnvSet("PREFIX_1",               "Spk-030RFC:RFS-EPR-210")
epicsEnvSet("DEVICE_IP_1",            "spk3-rf2-ep.tn.esss.lu.se")
epicsEnvSet("I2C_COMM_PORT_1",        "AK_I2C_COMM_1")

# Leakage current limits
epicsEnvSet("LEAK_HIHI_1",            "100.0")
epicsEnvSet("LEAK_HIGH_1",            "90.0")
epicsEnvSet("LEAK_LOW_1",             "0.0")
epicsEnvSet("LEAK_LOLO_1",            "0.0")

# Threshold current limits
epicsEnvSet("THRS_HIHI_1",            "100.0")
epicsEnvSet("THRS_HIGH_1",            "90.0")
epicsEnvSet("THRS_LOW_1",             "0.0")
epicsEnvSet("THRS_LOLO_1",            "0.0")

drvAsynIPPortConfigure($(I2C_COMM_PORT_1),"$(DEVICE_IP_1):1002")

# TMP temperature sensor
iocshLoad("$(xtpico_DIR)/tmp100.iocsh", "PREFIX=$(PREFIX_1), IP_PORT=$(I2C_COMM_PORT_1), N=1, NAME=$(R_TMP100), COUNT=1, INFOS=0x49")

# EEPROM
iocshLoad("$(xtpico_DIR)/m24m02.iocsh", "PREFIX=$(PREFIX_1), IP_PORT=$(I2C_COMM_PORT_1), N=1, NAME=$(R_M24M02), COUNT=1, INFOS=0x50")

# Port extender
iocshLoad("$(xtpico_DIR)/tca9555.iocsh", "PREFIX=$(PREFIX_1), IP_PORT=$(I2C_COMM_PORT_1), N=1, NAME=$(R_TCA9555), COUNT=1, INFOS=0x21")

# Voltage and current monitor
iocshLoad("$(xtpico_DIR)/ltc2991.iocsh", "PREFIX=$(PREFIX_1), IP_PORT=$(I2C_COMM_PORT_1), N=1, NAME=$(R_LTC2991), COUNT=1, INFOS=0x48")

# Digital rheostat
iocshLoad("$(xtpico_DIR)/ad527x.iocsh", "PREFIX=$(PREFIX_1), IP_PORT=$(I2C_COMM_PORT_1), N=1, NAME=$(R_AD527X), COUNT=1, INFOS=0x2E")

# Set up e-pickup specific db
dbLoadRecords("$(E3_CMD_TOP)/e-pickup.db", "P=$(PREFIX_1), R_LTC2991=$(R_LTC2991), R_TMP100=$(R_TMP100), R_TCA9555=$(R_TCA9555), R_AD527X=$(R_AD527X), LEAK_HIHI=$(LEAK_HIHI_1), LEAK_HIGH=$(LEAK_HIGH_1), LEAK_LOW=$(LEAK_LOW_1), LEAK_LOLO=$(LEAK_LOLO_1), THRS_HIHI=$(THRS_HIHI_1), THRS_HIGH=$(THRS_HIGH_1), THRS_LOW=$(THRS_LOW_1), THRS_LOLO=$(THRS_LOLO_1)")

# Hardware Initial config
## IO config
afterInit("dbpf $(PREFIX_1)$(R_TCA9555)-DirPin0 1")    # Input : Test Monitoring
afterInit("dbpf $(PREFIX_1)$(R_TCA9555)-DirPin1 1")    # Input : Interlock OFF
afterInit("dbpf $(PREFIX_1)$(R_TCA9555)-DirPin2 1")    # Input : Interlock ON
afterInit("dbpf $(PREFIX_1)$(R_TCA9555)-DirPin7 0")    # Output: test LED

## Rheostat config
afterInit("dbpf $(PREFIX_1)$(R_AD527X)-Type 0")        # AD5272 Type
afterInit("dbpf $(PREFIX_1)$(R_AD527X)-MaxRes 0")      # AD5272 Max resistance: 20k

pvlistFromInfo("ARCHIVE_THIS", "$(IOCNAME):ArchiverList")

iocInit

